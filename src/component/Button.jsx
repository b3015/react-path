import clsx from 'clsx';

export default function Button(props) {
    const { children, text, className = 'bg-blue-600' } = props;
    return (
        <>
            <button
                {...props}
                className={clsx(
                    className,
                    `[&>svg]:w-5 bg-blue-600 text-white px-4 py-2 rounded center p-px inline-flex justify-center whitespace-nowrap`,
                )}
            >
                {props.children || props.text}
            </button>
        </>
    );
}
