import { forwardRef } from 'react';

const Input = forwardRef(({  type = 'text', children, className, ...props },ref) => {

    return (
        <>
            <label
                className={`input input-bordered flex items-center gap-2 my-4 ${className}`}
            >
                <input {...props} className='grow' ref={ref}  type={type} />
                {children}
            </label>
        </>
    );
});

export default Input;


