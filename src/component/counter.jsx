import { useState } from 'react';
import Button from './Button';

export default function counter({ inisialValue }) {
    //   const [name, setName] = useState('Aditya')

    const [counter, setCounter] = useState(inisialValue);

    function handleClick() {
        // const data = counter + 1;
        const data = (x) => x + 1;
        setCounter(data);
        console.log({ counter, data });
    }

    function hanclearMinus() {
        const data = (x) => x - 1;
        setCounter(data);
        console.log({ counter, data });
    }

    return (
        <>
            {counter}
            <Button
                onClick={() => {
                    handleClick();
                    handleClick();
                    handleClick();
                }}
            >
                Add 3{' '}
            </Button>
            <Button onClick={hanclearMinus} className={'mt-4 bg-red-300'}>
                minus
            </Button>
        </>
    );
}
