function Card({ children, className, title, footer, description }) {
    return (
        <>
            <div className='max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700'>

                <a href='#'>
                    <h5 className='mb-2 text-2xl font-semibold tracking-tight text-gray-900 dark:text-white'>
                        {title}
                    </h5>
                </a>
                <p
                    className={`mb-3 font-normal text-gray-500 dark:text-gray-400  ${className} `}
                ></p>
                {children}
                <div className={'bg-slate-50'}>{footer}</div>
            </div>
        </>
    );
}

function footer({ children }) {
    return <div className={'bg-slate-50 '}>{children}</div>;
}
function Title({ children }) {
    return <p>{children}</p>;
}

Card.footer = footer;
Card.title = Title;

export default Card;
