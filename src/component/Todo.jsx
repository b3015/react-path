import { useEffect, useState } from 'react';
import Button from './Button';
import Card from './card';
import Input from './component-form/input';

export default function Todo() {
    const [newTaks, setNewTaks] = useState('');
    const [taks, setTaks] = useState([]);

    function handleAddTaks(e) {
        e.preventDefault();
        const data = {
            id: Math.floor(Math.random() * Date.now()),
            name: newTaks,
            complate: false,
        };
        setTaks((prevTasks) => [...prevTasks, data]);

        const updatedTasks = [...taks, data];
        localStorage.setItem('taks', JSON.stringify(updatedTasks));
        setNewTaks('');
    }

    function handRemove(id) {
        const removeTaks = taks.filter((tasks) => tasks.id !== id);
        setTaks(removeTaks);
        localStorage.setItem('taks', JSON.stringify(removeTaks));
    }

    function findTodo(id) {
        return taks.find((tasks) => tasks.id === id);
    }

    function handComplate(id) {
        const updatedTask = findTodo(id);
        if (updatedTask) {
            updatedTask.complate = true;
            const updatedTasks = [...taks];

            const index = updatedTasks.findIndex((task) => task.id === id);
            updatedTasks[index] = updatedTask;
            setTaks(updatedTasks);
            localStorage.setItem('taks', JSON.stringify(updatedTasks));
        }
    }

    useEffect(() => {
        const tesks = JSON.parse(localStorage.getItem('taks'));
        setTaks(tesks);
    }, []);

    return (
        <>
            <Card>
                <Card.title>Aditya</Card.title>
                <form>
                    <div className={'flex items-center gap-x-2'}>
                        <Input
                            className={'text-white'}
                            value={newTaks}
                            onChange={(e) => setNewTaks(e.target.value)}
                            name={'search'}
                        ></Input>
                        <Button onClick={handleAddTaks}>Add Taks</Button>
                    </div>
                </form>

                {taks.length > 0 ? (
                    <ol className={'space-y-2'}>
                        {taks.map((tasks) => (
                            <li
                                key={tasks.id}
                                className={
                                    'text-black flex items-center justify-between'
                                }
                            >
                                <div className={'flex items-center'}>
                                    <p className={'text-white p-1'}>
                                        {tasks.name}
                                    </p>
                                    <span className={'text-white'}>
                                        {tasks.complate
                                            ? 'Complate'
                                            : 'Incomplate'}
                                    </span>
                                </div>

                                <div
                                    className={'flex items-center gap-x-2 my-4'}
                                >
                                    <Button
                                        onClick={() => handComplate(tasks.id)}
                                        className={
                                            'px-3 py-1 text-xs bg-green-600'
                                        }
                                    >
                                        complated
                                    </Button>
                                    <Button
                                        onClick={() => handRemove(tasks.id)}
                                        className={
                                            'px-3 py-1 text-xs bg-gray-400'
                                        }
                                    >
                                        remove
                                    </Button>
                                </div>
                            </li>
                        ))}
                    </ol>
                ) : null}

                <Card.footer>
                    <p className={'text-black'}>you have {taks.length}</p>
                </Card.footer>
            </Card>
        </>
    );
}
