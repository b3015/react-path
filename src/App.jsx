import { useEffect, useRef, useState } from 'react';
import PlaseCententCenter from './component/PlaseContentCenter';
import Button from './component/Button';
import { IconBrandGithub, IconUserCheck, IconMail } from '@tabler/icons-react';

import Card from './component/card';
import Input from './component/component-form/input';

import useJoke from './component/hooks/UseJoke';

export default function App(props) {
    const [name, setName] = useState('')
    const joke = useJoke(name)
    const nameRef = useRef()

    const generateJoke = (e) => {
        e.preventDefault();
        setName(nameRef.current.value)
    }

    return (
        <>
            <PlaseCententCenter>
                <Card>
                    <Card.title>Joke</Card.title>
                        <p className={'mb-4'}>
                            {joke.value}
                        </p>

                    <Input ref={nameRef}  ></Input>
                    <Card.footer>
                        <Button onClick={generateJoke}>Generate Joke</Button>
                    </Card.footer>
                </Card>


            </PlaseCententCenter>
        </>
    );
}
